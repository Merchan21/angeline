package facci.angelinepico.projectmovil.Fragments;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import facci.angelinepico.projectmovil.R;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A simple {@link Fragment} subclass.
 */
public class AjustesFragment extends SupportMapFragment implements OnMapReadyCallback {

    private GoogleMap mMap;

    public AjustesFragment() {
    }

    public static AjustesFragment newInstance() {
        return new AjustesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        int stats = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getContext());

        if (stats == ConnectionResult.SUCCESS){

            SupportMapFragment mapFragment = (SupportMapFragment) getFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(stats,(Activity)getContext(),10);
            dialog.show();
        }


        return root;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        UiSettings uiSettings = mMap.getUiSettings();


        // Add a marker in Sydney and move the camera
        LatLng asd = new LatLng(-0.9529773991390442, -80.74553914419684);
        mMap.addMarker(new MarkerOptions().position(asd).title("Aqui estoy"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(asd));
        float zoomlevel = 16;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(asd, zoomlevel));
    }

}
