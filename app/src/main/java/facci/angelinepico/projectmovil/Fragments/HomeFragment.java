package facci.angelinepico.projectmovil.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import facci.angelinepico.projectmovil.Adapter.AdapterCardView;
import facci.angelinepico.projectmovil.Home;
import facci.angelinepico.projectmovil.Modelo.modelsCardView;
import facci.angelinepico.projectmovil.Splash;
import facci.angelinepico.projectmovil.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<modelsCardView> modelsCardViews;
    private AdapterCardView adapterCardView;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View view = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        modelsCardViews = new ArrayList<>();
        modelsCardViews.add(new modelsCardView(R.drawable.listass,"data"));
        modelsCardViews.add(new modelsCardView(R.drawable.yea,"data"));
        modelsCardViews.add(new modelsCardView(R.drawable.listass,"data"));
        modelsCardViews.add(new modelsCardView(R.drawable.yea,"data"));
        modelsCardViews.add(new modelsCardView(R.drawable.listass,"data"));
        modelsCardViews.add(new modelsCardView(R.drawable.yea,"data"));
        modelsCardViews.add(new modelsCardView(R.drawable.listass,"data"));
        modelsCardViews.add(new modelsCardView(R.drawable.yea,"data"));
        modelsCardViews.add(new modelsCardView(R.drawable.listass,"data"));
        modelsCardViews.add(new modelsCardView(R.drawable.yea,"data"));
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(layoutManager);
        adapterCardView = new AdapterCardView(getActivity(),modelsCardViews);
        recyclerView.setAdapter(adapterCardView);
        return view;

    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main_nav, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            startActivity(new Intent(getActivity(), Splash.class));
        }
        return super.onOptionsItemSelected(item);
    }
}


