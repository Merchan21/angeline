package facci.angelinepico.projectmovil.Modelo;

public class modelsCardView {
    private int img;
    private String details;

    public modelsCardView(int img, String details) {
        this.img = img;
        this.details = details;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
