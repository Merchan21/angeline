package facci.angelinepico.projectmovil.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import facci.angelinepico.projectmovil.Modelo.modelsCardView;
import facci.angelinepico.projectmovil.R;

public class AdapterCardView extends RecyclerView.Adapter<AdapterCardView.myAdapter> {

    private Activity activity;
    private List<modelsCardView> modelsCardViews;

    public AdapterCardView(Activity activity, List<modelsCardView> modelsCardViews) {
        this.activity = activity;
        this.modelsCardViews = modelsCardViews;
    }




    @NonNull
    @Override
    public myAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardviewinflate,parent,false);
        return new myAdapter(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myAdapter holder, int position) {
        holder.textView.setText(modelsCardViews.get(position).getDetails());
        Picasso.get().load(modelsCardViews.get(position).getImg()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return modelsCardViews.size();
    }

    class myAdapter extends RecyclerView.ViewHolder{

        private ImageView imageView;
        private TextView textView;

        public myAdapter(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imgCard);
            textView = itemView.findViewById(R.id.textCard);
        }


    }
}
