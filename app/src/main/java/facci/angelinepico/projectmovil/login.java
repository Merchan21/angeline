package facci.angelinepico.projectmovil;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class login extends AppCompatActivity {

    FirebaseAuth auth;

    ProgressDialog pd;
    Button btnlogin, mapa;
    TextView email, contrasena;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        Button btn = (Button) findViewById(R.id.btn_registro);
        btnlogin = findViewById(R.id.btn_inicio);
        email = findViewById(R.id.email);
        mapa = findViewById(R.id.mapa);
        contrasena = findViewById(R.id.contrasena);
        auth = FirebaseAuth.getInstance();

        mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(login.this, MapsActivity.class));

            }
        });


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), registro.class);
                startActivityForResult(intent, 0);
            }
        });

        pd = new ProgressDialog(this);


        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String correo = email.getText().toString();
                String clave = contrasena.getText().toString();


                if (isValidEmail(correo) && validarContraseña()) {
                    String contraseña = contrasena.getText().toString();
                    auth.signInWithEmailAndPassword(correo, contraseña)
                            .addOnCompleteListener(login.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {

                                        Toast.makeText(login.this, "se ingreso correctamente", Toast.LENGTH_SHORT).show();
                                        nextActivity();
                                    } else {

                                        Toast.makeText(login.this, "ocurrio un error al inicio de sesion", Toast.LENGTH_SHORT).show();

                                    }
                                }
                            });
                }
            }
        });
    }

    private void nextActivity() {
        startActivity(new Intent(login.this, principal.class));
        finish();
    }

    private boolean isValidEmail (CharSequence target){
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean validarContraseña (){
        String contraseña;
        contraseña = contrasena.getText().toString();
        if (contraseña.length() >=6){
            return true;
        }else return false;

    }
}